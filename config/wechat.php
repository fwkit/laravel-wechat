<?php
/**
 * @author   Fung Wing Kit <wengee@gmail.com>
 * @version  2018-11-01 15:04:12 +0800
 */
return [
    'default' => env('WECHAT_ACCOUNT', 'default'),

    'accounts' => [
        'default' => [
            'type' => env('WECHAT_TYPE', 'minapp'),
            'appId' => env('WECHAT_APP_ID', 'YOUR_APP_ID'),
            'appSecret' => env('WECHAT_APP_SECRET', 'YOUR_APP_SECRET'),
            'token' => env('WECHAT_TOKEN', 'YOUR_TOKEN'),
            'encodingAESKey' => env('WECHAT_ENCODING_AES_KEY', 'YOUR_ENCODING_AES_KEY'),
        ],
    ],
];
