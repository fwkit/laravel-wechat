<?php
/**
 * @author   Fung Wing Kit <wengee@gmail.com>
 * @version  2018-11-02 17:04:57 +0800
 */
namespace fwkit\LaravelWechat\Minapp;

use fwkit\LaravelWechat\ClientBase;

class Client extends ClientBase
{
    protected static $componentList = [
        'analysis' => Components\Analysis::class,
        'media'    => Components\Media::class,
        'message'  => Components\Message::class,
        'nearby'   => Components\Nearby::class,
        'oauth'    => Components\OAuth::class,
        'plugin'   => Components\Plugin::class,
        'qrcode'   => Components\QrCode::class,
        'security' => Components\Security::class,
        'template' => Components\Template::class,
        'token'    => Components\Token::class,
    ];

    protected $baseUri = 'https://api.weixin.qq.com/';
}
