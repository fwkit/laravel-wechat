<?php
/**
 * @author   Fung Wing Kit <wengee@gmail.com>
 * @version  2018-11-02 17:58:36 +0800
 */
namespace fwkit\LaravelWechat\Message;

class UnknownMessage extends MessageBase
{
}
