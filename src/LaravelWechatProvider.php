<?php
/**
 * @author   Fung Wing Kit <wengee@gmail.com>
 * @version  2018-11-01 15:48:23 +0800
 */
namespace fwkit\LaravelWechat;

use Illuminate\Support\ServiceProvider;

class LaravelWechatProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/wechat.php',
            'wechat'
        );

        $this->app->alias('fwkit\LaravelWechat\Manager', 'wechat');
    }

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/wechat.php' => base_path('config/wechat.php'),
        ]);
    }
}
