<?php
/**
 * @author   Fung Wing Kit <wengee@gmail.com>
 * @version  2018-11-01 14:48:16 +0800
 */
namespace fwkit\LaravelWechat;

class OfficialError extends \Exception
{
}
