<?php
/**
 * @author   Fung Wing Kit <wengee@gmail.com>
 * @version  2018-11-02 16:09:18 +0800
 */
namespace fwkit\LaravelWechat\Concerns;

trait HasAccessToken
{
    protected static $tokenGetter;

    protected $accessToken;

    public function getAccessToken(bool $forceUpdate = false)
    {
        if ($this->accessToken && !$forceUpdate) {
            return $this->accessToken;
        }

        $accessToken = null;
        $appId = $this->appId;
        $cacheKey = 'wechat-access-token-' . $appId;

        if (method_exists($this, 'getCache') && !$forceUpdate) {
            $accessToken = $this->getCache($cacheKey);
        }

        if (empty($accessToken) && static::$tokenGetter && is_callable(static::$tokenGetter)) {
            $accessToken = call_user_func(static::$tokenGetter, $appId);
        }

        if (empty($accessToken)) {
            $token = $this->component('token');
            if ($token) {
                try {
                    $res = $token->getAccessToken();
                    $accessToken = $res->get('accessToken', null);
                    if (method_exists($this, 'setCache')) {
                        $ttl = (int) max(1, $res->get('expiresIn', 0) - 600);
                        $this->setCache($cacheKey, $accessToken, $ttl);
                    }
                } catch (\Exception $e) {
                }
            }
        }

        $this->accessToken = $accessToken;
        return $accessToken;
    }

    public static function setTokenGetter(callable $func)
    {
        static::$tokenGetter = $func;
    }
}
