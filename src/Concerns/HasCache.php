<?php
/**
 * @author   Fung Wing Kit <wengee@gmail.com>
 * @version  2018-11-01 16:39:04 +0800
 */
namespace fwkit\LaravelWechat\Concerns;

trait HasCache
{
    protected function setCache(string $key, $value, int $ttl = 0)
    {
        $ttl = floor($ttl / 60);
        return app('cache')->put($key, $value, $ttl);
    }

    protected function getCache(string $key)
    {
        return app('cache')->get($key);
    }
}
