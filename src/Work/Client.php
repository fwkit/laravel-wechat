<?php
/**
 * @author   Fung Wing Kit <wengee@gmail.com>
 * @version  2018-11-02 17:06:22 +0800
 */
namespace fwkit\LaravelWechat\Work;

use fwkit\LaravelWechat\ClientBase;

class Client extends ClientBase
{
    protected $baseUri = 'https://qyapi.weixin.qq.com/';
}
