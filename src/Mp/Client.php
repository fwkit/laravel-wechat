<?php
/**
 * @author   Fung Wing Kit <wengee@gmail.com>
 * @version  2018-11-02 17:06:17 +0800
 */
namespace fwkit\LaravelWechat\Mp;

use fwkit\LaravelWechat\ClientBase;

class Client extends ClientBase
{
    protected $baseUri = 'https://api.weixin.qq.com/';
}
